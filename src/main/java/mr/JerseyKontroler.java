package mr;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Path("/")
public class JerseyKontroler {

    @GET
    @Path("/list")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Bin> lista() {
        return Arrays.asList(build(), build());
    }

    @GET
    @Path("/id/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Bin byId(@PathParam("id") Integer id) {
        return build();
    }

    @GET
    @Path("/sample")
    @Produces(MediaType.TEXT_PLAIN)
    public String sample() {
        return RandomStringUtils.randomAlphanumeric(100);
    }

    private Bin build() {
        Bin b = new Bin();
        b.setData(new Date());
        b.setId(RandomUtils.nextInt());
        b.setImie(StringUtils.capitalize(RandomStringUtils.randomAscii(10)));
        return b;
    }
}
