package mr;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/")
public class SpringKontroler {

    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    public List<Bin> lista() {
        return Arrays.asList(build(), build());
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    public Bin byId(@PathVariable(value = "id") Integer id) {
        return build();
    }

    @RequestMapping(value = "/sample", produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public String sample() {
        return RandomStringUtils.randomAlphanumeric(100);
    }

    private Bin build() {
        Bin b = new Bin();
        b.setData(new Date());
        b.setId(RandomUtils.nextInt());
        b.setImie(StringUtils.capitalize(RandomStringUtils.randomAscii(10)));
        return b;
    }
}
